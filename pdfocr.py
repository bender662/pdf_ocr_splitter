from PyPDF2 import PdfFileWriter, PdfFileReader,PdfFileMerger
import os,textract,time,pytesseract

#declare variable

OrigPdf = input("Numele pdf total\n")
startNum = int(input("Numarul de inceput al fiselor\n"))

#function to split my original pdf in sections
def file_splitter(start,stop,startNum):
    pdf_file = PdfFileReader(OrigPdf)
    new_pdf = PdfFileWriter()
    if "pdf_split" not in os.listdir():
        os.mkdir("pdf_split")
    for page in range(start,stop):
        new_pdf.addPage(pdf_file.getPage(page))
        with open("pdf_split/" + str(startNum) + ".pdf",'wb') as create_file:
            new_pdf.write(create_file)
                       


#opening original pdf for checking
pdf_file = PdfFileReader(OrigPdf)
page_numbers = []

for pages in range(pdf_file.getNumPages()):
    check_PDF = PdfFileWriter()
    check_PDF.addPage(pdf_file.getPage(pages))
    

    with open("current.pdf",'wb') as newpdf:
        check_PDF.write(newpdf)

    text = textract.process("current.pdf",method = 'tesseract')
    

    if "FISA DE DATE A IMOBILULUI" in text.decode():
        page_numbers.append(pages)
        print(page_numbers)
    os.remove("current.pdf")



for number in range(len(page_numbers)):
    try:
        file_splitter(page_numbers[number],page_numbers[number + 1],startNum)
    except IndexError:
        file_splitter(page_numbers[number],pdf_file.getNumPages(),startNum)
    startNum += 1
